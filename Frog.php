<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
<?php
require_once ("animal.php");
class Frog extends Animal {
    public $legs = 4;
    public function jump() {
        echo "hop hop"."<br>";
    }
}
?>
</body>
</html>